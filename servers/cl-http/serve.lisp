(in-package :cl-user)

(uiop:with-current-directory (*load-truename*)
  (load "../../build"))

(ql:quickload :cl-site-cl-http)

;;
;; Start aserve, publish site directory & pages, set default package.
;;
(cl-site-cl-http:start-server)
(cl-site-cl-http:publish-cl-site)
(setq *package* (find-package :cl-site))

(format t "

This message from cl-site/servers/cl-http/serve.lisp should report the url & port 
that the user may visit to browse the site.

")

